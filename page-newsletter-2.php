<?php  
/*
* Template Name: Newsletter
*/
?>
<?php get_header(); ?>

<div class="content-section prd-detail">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="">Home <i class="fa fa-chevron-right" aria-hidden="true"></i></a></li>
                <li><a href="">COMPANY INFO</a></li>
                <li><a href="" class="active"><i class="fa fa-chevron-right" aria-hidden="true"></i>NEWSLETTER
</a></li>
            </ul>
            
            
          <div class="news-letter">
          <h2><?php the_field('news_title'); ?></h2>
          <div class="news-lette-left">
          <div class="col-sm-2">
          <div class="num">
          <span><?php echo date('M'); echo date('Y'); ?></span>
          </div>
          <div>
                  <!-- this is for the post publish on this page  --> 
                  <?php
                          // the query
                          $the_query = new WP_Query(array(
                              'category_name' => 'newsletter-2',
                              'post_status' => 'publish',
                              'posts_per_page' => 5,
                          ));
                  ?>

                  <?php if ($the_query->have_posts()) : ?>
                      <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                          <!-- the post title -->
                          <h3><?php the_title(); ?></h3>
                          
                          <!-- the post content -->
                          <p><?php the_content(); ?></p>
                          <br>
                      <?php endwhile; ?>
                      <?php wp_reset_postdata(); ?>

                  <?php else : ?>
                      <p><?php __('No Posts'); ?></p>
                  <?php endif; ?>

          </div>
          
          </div>
          
          <div class="col-sm-3">
          <h3><?php the_field('issue_title'); ?></h3>
 
              <!-- Repeater -->
          <?php if( have_rows('issue') ) : ?>
              <?php  while ( have_rows('issue') ) : the_row(); ?>
                    <div class="col-ms-2">
                    <h4><?php the_sub_field('issue_title'); ?></h4>
                    <img src="<?php the_sub_field('issue_image');  ?>" alt="">
                    <p><?php the_sub_field('issue_content');  ?><a href=""><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></a></p>
                    </div>
              <?php endwhile; ?>
          <?php endif; ?>

          <div class="subscribe">
          <h3><?php the_field('subscribe_title'); ?></h3>
          <span><?php the_field('subscribe_content'); ?></span>
          <div class="date">
            <form accept="#" method="post">
              <?php echo do_shortcode( '[contact-form-7 id="186" title="Untitled"]' ); ?>
            </form> 
          </div>
          </div>

          <div class="privacy-commitment">
          <strong><?php the_field('privacy_title'); ?></strong>
          <p><?php the_field('privacy_content'); ?></p>

          </div>
          </div>
          </div>
          </div>  
        </div>
    </div>

<?php get_footer(); ?>