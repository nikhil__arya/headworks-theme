<?php  
/*
* Template Name: Contact Us
*/
?>
<?php get_header(); ?>

<div class="content-section prd-detail">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="">Home</a></li>
                <li><a href="" class="active"><i class="fa fa-chevron-right" aria-hidden="true"></i>contact us
</a></li>
            </ul>
            
            
          <div class="contact-us">
          <h2><?php the_title(); ?></h2>
          <div class="con-left">
          
          <div class="col-text">
          <h4><?php the_field('headquater_title'); ?></h4>
          <div class="text-num">
          <i class="fa fa-map-marker" aria-hidden="true"></i><?php the_field('headquater_address'); ?>
          </div>
          
          <!-- Repeater for Telephone numbers -->
          <?php if( have_rows('phone') ) : ?>
    		<?php  while ( have_rows('phone') ) : the_row(); ?>
    			<div class="text-num"><i class="fa fa-phone-square" aria-hidden="true"></i>
					<a href="tel:<?php the_sub_field('phone_no'); ?>"><?php the_sub_field('phone_no'); ?> (<?php the_sub_field('phone_type'); ?>)</a>
          		</div>
   	    	<?php endwhile; ?>
		<?php endif; ?>

          <div class="text-num"><i class="fa fa-envelope" aria-hidden="true"></i>
<a href=""><?php the_field('headquater_website'); ?></a>
          </div>
          <p><?php the_field('headquater_content'); ?></p>
          </div>

          <!-- Repeater for Offices -->
      <?php if( have_rows('offices') ) : ?>
		<?php  while ( have_rows('offices') ) : the_row(); ?>
    		 <div class="col-text">
		          <h4><?php the_sub_field('office_name'); ?></h4>
		          <div class="text-num"><i class="fa fa-phone-square" aria-hidden="true"></i>
					<a href="tel:17136476667"><?php the_sub_field('office_phone'); ?></a>
		          </div>
		          <div class="text-num"><i class="fa fa-envelope" aria-hidden="true"></i>
					<a href=""><?php the_sub_field('office_website'); ?></a>
		          </div>
          	</div>
	    	<?php endwhile; ?>
	  <?php endif; ?>

  
          <div class="service-text">
          <h3><?php the_field('service_title') ?></h3>
	       	<?php the_field('service_content'); ?>
          </div>
          
          
          </div>
          <div class="con-right">
          
<div class="map"><img src="<?php the_field('map_image'); ?>" alt=""></div>
          </div>
          </div>  
        </div>
    </div>

<?php get_footer(); ?>