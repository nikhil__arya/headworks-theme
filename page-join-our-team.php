<?php 
/*
* Template Name: Join Our Team
*/
?>
<?php get_header(); ?>

<div class="content-section prd-detail">
        <data></data><div class="container">
            <ul class="breadcrumbs">
                <li><a href="">Home<i class="fa fa-chevron-right" aria-hidden="true"></i></a></li>
                <li><a href="">company info</a></li>
                <li><a href="" class="active"><i class="fa fa-chevron-right" aria-hidden="true"></i>Team</a></li>
            </ul>
            
         
         
<div class="careers-detail">
<h2><?php the_field('meet_our_team_title'); ?></h2>
<div class="our-team">
<ul>


	<?php if( have_rows('meet_our_team') ) : ?>
    	<?php  while ( have_rows('meet_our_team') ) : the_row(); ?>
    		<li>
				<img src="<?php echo the_sub_field('member_image'); ?>" alt="">
				<h3><?php echo the_sub_field('member_name'); ?></h3>
				<span><?php echo the_sub_field('member_designation'); ?></span>
				<p><?php echo the_sub_field('member_content'); ?></p>
			</li>
   	    <?php endwhile; ?>
	  
	<?php endif; ?>

</ul>

</div>
</div>
</div>
</div>


<?php get_footer(); ?>