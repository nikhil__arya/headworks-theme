<?php  
/*
* Template Name: Products
*/
?>
<?php get_header(); ?>

 <div class="content-section prd-detail">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="">Home <i class="fa fa-chevron-right" aria-hidden="true"></i></a></li>
                <li></li>
                <li><a href="">Mechanical Equipment</a></li>
                <li><a href="" class="active"><i class="fa fa-chevron-right" aria-hidden="true"></i>Headworks Bar Screen
</a></li>
            </ul>
            <div class="col-sm">
                <h2><?php the_field('product_title'); ?></h2>
                <p><?php the_field('product_content'); ?></p>
                <a href="" class="btn-sm"><i class="fa fa-plus-circle" aria-hidden="true"></i>MORE</a>
            </div>
            <div class="col-sm">
             <img src="<?php echo the_field('product_image'); ?>" alt="">
                
                <div class="date">
                  <strong>MBBR / IFAS  Overview</strong>
                </div>
            </div>
        </div>
    </div>
    <div class="tabs-box">
        <div class="container">
            <div id="product-tab">
                <ul class="resp-tabs-list">
                 
                    <!-- Repeater for list  -->
                        <?php if( have_rows('resp_product_list') ) : ?>
                            <?php  while ( have_rows('resp_product_list') ) : the_row(); ?>
                               <li><?php the_sub_field('list'); ?></li>
                            <?php endwhile; ?>
                        <?php endif; ?>

                </ul>
                <div class="resp-tabs-container">
                 <div>
                 <div class="product">
                 <ul>
               

                        <!-- Repeater for Product  -->
                        <?php if( have_rows('product') ) : ?>
                            <?php  while ( have_rows('product') ) : the_row(); ?>
                               <li>
                                 <img src="<?php the_sub_field('product_image'); ?>" alt="">
                                 <div class="prd-text">
                                 <span><?php the_sub_field('product_title'); ?></span>
                                 <p><?php the_sub_field('product_content'); ?></p>
                                 </div>
                                </li>
                            <?php endwhile; ?>
                        <?php endif; ?>

                  
                 </ul>
                 
                 </div>
                 
                 </div>  
                 <div>
                 <div class="product">
                 <ul>
                 <!-- Repeater for Product  -->
                        <?php if( have_rows('product') ) : ?>
                            <?php  while ( have_rows('product') ) : the_row(); ?>
                               <li>
                                 <img src="<?php the_sub_field('product_image'); ?>" alt="">
                                 <div class="prd-text">
                                 <span><?php the_sub_field('product_title'); ?></span>
                                 <p><?php the_sub_field('product_content'); ?></p>
                                 </div>
                                </li>
                            <?php endwhile; ?>
                        <?php endif; ?>
                  
                 </ul>
                 
                 </div>
                 
                 </div>
                 <div>
                 <div class="product">
                 <ul>
                                  <!-- Repeater for Product  -->
                        <?php if( have_rows('product') ) : ?>
                            <?php  while ( have_rows('product') ) : the_row(); ?>
                               <li>
                                 <img src="<?php the_sub_field('product_image'); ?>" alt="">
                                 <div class="prd-text">
                                 <span><?php the_sub_field('product_title'); ?></span>
                                 <p><?php the_sub_field('product_content'); ?></p>
                                 </div>
                                </li>
                            <?php endwhile; ?>
                        <?php endif; ?>
                 </ul>
                 
                 </div>
                 
                 </div>
                 <div>
                 <div class="product">
                 <ul>
                                  <!-- Repeater for Product  -->
                        <?php if( have_rows('product') ) : ?>
                            <?php  while ( have_rows('product') ) : the_row(); ?>
                               <li>
                                 <img src="<?php the_sub_field('product_image'); ?>" alt="">
                                 <div class="prd-text">
                                 <span><?php the_sub_field('product_title'); ?></span>
                                 <p><?php the_sub_field('product_content'); ?></p>
                                 </div>
                                </li>
                            <?php endwhile; ?>
                        <?php endif; ?>                 
                    </ul>
                 
                 </div>
                 
                 </div>
                 <div>
                 <div class="product">
                 <ul>
                                  <!-- Repeater for Product  -->
                        <?php if( have_rows('product') ) : ?>
                            <?php  while ( have_rows('product') ) : the_row(); ?>
                               <li>
                                 <img src="<?php the_sub_field('product_image'); ?>" alt="">
                                 <div class="prd-text">
                                 <span><?php the_sub_field('product_title'); ?></span>
                                 <p><?php the_sub_field('product_content'); ?></p>
                                 </div>
                                </li>
                            <?php endwhile; ?>
                        <?php endif; ?>                 
                 </ul>
                 
                 </div>
                 
                 </div>
                 <div>
                 <div class="product">
                 <ul>
                                  <!-- Repeater for Product  -->
                        <?php if( have_rows('product') ) : ?>
                            <?php  while ( have_rows('product') ) : the_row(); ?>
                               <li>
                                 <img src="<?php the_sub_field('product_image'); ?>" alt="">
                                 <div class="prd-text">
                                 <span><?php the_sub_field('product_title'); ?></span>
                                 <p><?php the_sub_field('product_content'); ?></p>
                                 </div>
                                </li>
                            <?php endwhile; ?>
                        <?php endif; ?>
                 </ul>
                 
                 </div>
                 
                 </div>
                 
                 
                 
                 
                 
                 
                </div>
            </div>
        </div>
        </div>

<?php get_footer(); ?>