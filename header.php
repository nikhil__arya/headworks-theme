<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title><?php echo bloginfo( 'name' ); ?></title>
    <?php //include 'meta.html' ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo ot_get_option('fevicon'); ?>">
	<link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo get_template_directory_uri(); ?>/css/tabs.css" type="text/css" rel="stylesheet" />
	<link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?php echo get_template_directory_uri(); ?>/css/style-responsive.css" rel="stylesheet" type="text/css" media="all" />

    <?php wp_head(); ?>
</head>


<body class="home" <?php body_class(); ?>>

<header>
<div class="header-bg"> 
<div class="container">
<a href="index.html" class="logo">
<img src="<?php echo ot_get_option('logo'); ?>" alt="">	
</a>
<div class="right-pull">
	<?php wp_nav_menu(array('menu'=>'Menu Header','container'=>'div','container_class'=>'nav-top'));?>
<div class="search-from">
<form>
<input type="" name="" placeholder="Search"><a href=""><i class="fa fa-search" aria-hidden="true"></i></a>
	
</form>
</div>	

</div>
</div>
</div>
<a href="javascript:void(0)" class="menu-toggle">Menu</a>
<nav>
	<?php wp_nav_menu(array('menu'=>'Main Menu','container'=>'div','container_class'=>'container'));?>
</nav>
</header>