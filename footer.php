<footer>
    <h3><?php echo ot_get_option('footer_title'); ?><span>TM</span></span></h3>
    <div class="footer-middle" style='background:url("<?php echo ot_get_option('footer_image'); ?>")'>
        <div class="container">
            <div class="col-footer">
                <h3>LINKS</h3>
                <ul>
                    <!-- Header menus in footer -->                 
                    <?php wp_nav_menu(array('menu'=>'Menu Header','container'=>'ul','container_class'=>''));?>
                </ul>
                <ul>
                    <!-- Main menus in footer with (depth 1: not to show child items)  --> 
                    <?php wp_nav_menu(array('menu'=>'Main Menu','container'=>'ul','container_class'=>'','depth'=> 1));?>
                </ul>
            </div>
            <div class="col-footer">
            <address>
                <h3><?php echo ot_get_option('contact_title'); ?></h3>
                <p><?php echo ot_get_option('contact_address'); ?> 
                </p>
                <div class="phone-b"><i class="fa fa-phone" aria-hidden="true"></i>PHONE:<a href=""><?php echo ot_get_option('phone_no'); ?></a></div>
                <div class="phone-b"><i class="fa fa-fax" aria-hidden="true"></i>FAX:<a href=""><?php echo ot_get_option('fax_no'); ?></a></div>
                <div class="phone-b"><i class="fa fa-envelope" aria-hidden="true"></i><?php echo ot_get_option('contact_email'); ?></div>
                </address>
            </div>
             <div class="col-footer">
                <h3><?php echo ot_get_option('connect_title'); ?></h3>
                <div class="social">
                    <a href="<?php echo ot_get_option('facebook_link'); ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    <a href="<?php echo ot_get_option('twitter_link'); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    <a href="<?php echo ot_get_option('linkedin_link'); ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                    <a href="<?php echo ot_get_option('youtube_link'); ?>"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                </div>
                <div class="newsletter">
                <span><?php echo ot_get_option('newsletter_signup'); ?></span>
                <form action="#" method="post">
                <?php echo do_shortcode( '[contact-form-7 id="187" title="newsletter form"]'); ?>
                  
                </form> 
                </div>
            </div>
               
            </div>
        </div>
    </div>


   <div class="copyright">
   <div class="container">
        <p>
       <span>Copyright © <?php echo date('Y') ?> <?php echo bloginfo( 'name' ); ?></span><span></span>
        </p>
        <ul>
        <li><a href=""><?php echo ot_get_option('disclaimer'); ?></a></li>
        <li><a href=""><?php echo ot_get_option('site_map'); ?></a></li>
        <li><a href=""><?php echo ot_get_option('privacy_policy'); ?></a></li>  
        </ul>
    </div>
   </div> 
</footer>
<script src="http://code.jquery.com/jquery-1.7.2.min.js "></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/lightbox.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/tabs.js" type="text/javascript"></script> 
<script src="<?php echo get_template_directory_uri(); ?>/js/main.js" type="text/javascript"></script>
<?php wp_footer(); ?>
</body>

</html>