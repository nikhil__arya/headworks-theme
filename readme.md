 
This theme is for Headworks International.
 
The theme includes the following template files:
footer.php
functions.php
page.php - for static pages
page-company-info.php
header.php
index.php
page-home.php – for home page
page-contact-us.php
page-industrial.php
page-join-our-team.php
page-newsletter-2.php
page-products.php
page-resources.php
page-resp-only.php


 
The theme supports featured images, menus and widgets and uses them as follows:
 
 
Menus:
The Header menu and Main Menu is in header.php.
 
Styling:
No Styling.
 
Hooks
 
There are 3 action hooks:
- for css
-for js
-for registering navigation menus
 

Plugins Used:
-Option Tree 
-Contact Form 7
-Advanced Custom Fields
-Repeater
-Easy WP SMTP For mails in forms
-WP Mail SMTP


Advance Custom Fields:

There are custom fields for different pages.

