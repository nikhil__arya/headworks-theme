<?php  
/*
* Template Name: Resp Only
*/
?>
<?php get_header(); ?>

<div class="content-section prd-detail">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="">Home <i class="fa fa-chevron-right" aria-hidden="true"></i></a></li>
                <li><a href="">resources </a></li>
                <li><a href="" class="active"><i class="fa fa-chevron-right" aria-hidden="true"></i>reps only
</a></li>
            </ul>
            
            <div class="case_studies reps-only">
            <h2>Reps Only</h2>
            <ul>

		            <?php if( have_rows('download_section') ): ?>
					    <?php  while ( have_rows('download_section') ) : the_row(); ?>
				              <li>
					          <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
					          <span><?php the_sub_field('pdf_name'); ?></span>
					          <a href="" class="btn-sm-2"><?php the_sub_field('pdf_action'); ?><i class="fa fa-download" aria-hidden="true"></i></a>
					          </li>
						<?php endwhile; ?>
					<?php else : ?>
				<?php endif; ?>
				
            </ul>
</div>
            
        </div>
    </div>

<?php get_footer(); ?>