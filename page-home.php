<?php
/*
* Template Name: Home
*/ 
 ?>
<?php get_header(); ?>

<div class="home-banner">
  <div class="slick-bg">
     <div style='background:url("<?php the_field('banner_image'); ?>")'>
           <div class="banner-details">
            <div class="container">
              <div class="slider-text">
                <h1><?php the_field('banner_title'); ?></h1>
                <p><?php the_field('banner_content'); ?></p>
                <a href="" class="btn">learn MORE <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></a>
              </div>
            </div>
           </div>
     </div>
   
     <div>
          <div class="home-vidoes">
          <iframe src="https://www.youtube.com/embed/Sad9x4jOPXM" allowfullscreen="" frameborder="0"></iframe>
          </div>
     </div>
   
     <div>
      <div class="banner-details">
        <div class="container">
          <div class="slider-text">
          <h1><?php the_field('banner_title'); ?></h1>
                <p><?php the_field('banner_content'); ?></p>
          <a href="" class="btn">learn MORE <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></a>
          </div>
        </div>
      </div>
     </div>

   </div>
</div>

<div class="content-section home-about">
  <div class="container">
   
   <div class="col-ms">
  <!-- About Title -->
<?php if( get_field('about_title') ): ?>
    <?php the_field('about_title'); ?>
<?php endif; ?>
      <!-- About Content -->
  <?php if( get_field('about_content') ): ?>
    <?php the_field('about_content'); ?>
<?php endif; ?>

  
   </div> 
   <div class="col-ms col-ms-2">
       <!-- Testimonial Title -->
   <?php the_field('testimonial_title'); ?>
<div class="one-time">
<div>
        <!-- Testmonial Content -->
   <?php the_field('testimonial_content'); ?>
        <!-- Testmonial Author -->
   <a href=""><?php the_field('testimonial_author'); ?></a>
</div>
<div>
        <!-- Testimonial Content -->
   <?php the_field('testimonial_content'); ?>
        <!-- Testmonial Author -->
   <a href=""><?php the_field('testimonial_author'); ?></a>
</div>
<div>
       <!-- Testimonial Content -->
   <?php the_field('testimonial_content'); ?>
        <!-- Testmonial Author -->
   <a href=""><?php the_field('testimonial_author'); ?></a>
</div>
<div>
      <!-- Testimonial Content -->
   <?php the_field('testimonial_content'); ?>
      <!-- Testmonial Author -->
   <a href=""><?php the_field('testimonial_author'); ?></a>
</div>
<div>
        <!-- Testimonial Content -->
   <?php the_field('testimonial_content'); ?>
        <!-- Testmonial Author -->
   <a href=""><?php the_field('testimonial_author'); ?></a>
</div>
</div>
</div> 
    <!-- To get The Side bar -->

    <div class="col-ms col-ms-3">
      <h3><?php the_field('newsletter_title'); ?></h3>
      <div class="date">
        <samp><?php the_field('recent_newsletter'); ?></samp>
      <a href="" class="btn-sm2"><i class="fa fa-newspaper-o" aria-hidden="true"></i>READ Now <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></a>
      </div>

      <div class="date">
      <samp><?php the_field('sign_up_newsletter'); ?></samp>
     <form action="#" method="post">
      <?php echo do_shortcode( '[contact-form-7 id="187" title="newsletter form"]'); ?>
     </form> 
      </div>
    </div>
  </div>
</div>

<div class="content-section home-latest">
<div class="container">
    <!-- Latest Activites -->
    <?php the_field('latest_activities'); ?>
<ul class="responsive">
  <li>
      <!-- Product Image -->
    <?php the_field('product_image'); ?>
      <!-- Product Name -->
    <span><?php the_field('product_name'); ?></span>
      <!-- Product Content -->
    <?php the_field('product_content'); ?>
    
  </li>
  <li>
      <!-- Product Image -->
    <?php the_field('product_image'); ?>
      <!-- Product Name -->
    <span><?php the_field('product_name'); ?></span>
      <!-- Product Content -->
    <?php the_field('product_content'); ?>
    
  </li>
  <li>
      <!-- Product Image -->
    <?php the_field('product_image'); ?>
      <!-- Product Name -->
    <span><?php the_field('product_name'); ?></span>
      <!-- Product Content -->
    <?php the_field('product_content'); ?>
    
  </li>
</ul>
</div>

</div>

<div class="content-section home-vidoes">
  
<div class="container">
  <div class="col-left"><?php the_field('interview_video_url'); ?></div>
  <div class="col-right">
    <h3><?php the_field('interview_title'); ?></h3>
        <?php the_field('interview_content'); ?>
<a href="" class="btn">learn MORE<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></a>
</div>
</div>
</div>


<div class="content-section home-company">
    <!-- Repeater for advertize  -->
  <?php if( have_rows('slides') ): ?>
    <?php  while ( have_rows('slides') ) : the_row(); ?>
              <div class="col-ms"> 
                <a href=""><img src="<?php echo the_sub_field('adv_image'); ?>" alt=""></a>
                <span><?php the_sub_field('adv_content'); ?></span>
              </div>

    <?php endwhile; ?>

  <?php else : ?>
<?php endif; ?>

  
</div>

</div>
<?php get_footer(); ?>

