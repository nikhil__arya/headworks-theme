<?php 
/*
* Template Name: Resources
*/
?>
<?php get_header(); ?>

<div class="content-section prd-detail">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="">Home <i class="fa fa-chevron-right" aria-hidden="true"></i></a></li>
                <li><a href="">resources </a></li>
                <li><a href="" class="active"><i class="fa fa-chevron-right" aria-hidden="true"></i>case studies
</a></li>
            </ul>
            
            <div class="case_studies">
            <h2><?php echo the_field('case_studies_title'); ?></h2>
            <p><?php echo the_field('case_studies_content'); ?></p>
            <ul>

       
            <?php if( have_rows('case_studies') ) : ?>
    			<?php  while ( have_rows('case_studies') ) : the_row(); ?>
		    		<li>
						<div class="img-hover">
			            <img src="<?php the_sub_field('case_studies_image'); ?>" alt="">
			            <div class="hover">
			             <span><i class="fa fa-file" aria-hidden="true"></i>READ NOW</span>
			            </div>
			            </div>
			            <span><?php the_sub_field('case_studies_name'); ?></span>
			            <p><?php the_sub_field('case_studies_content'); ?></p>
					</li>
   	    		<?php endwhile; ?>
	 		<?php endif; ?>

            
            </ul>
</div>
            
        </div>
    </div>

<?php get_footer(); ?>