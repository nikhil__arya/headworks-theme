<?php  
/*
* Template Name: Industry
*/
?>
<?php get_header(); ?>

<div class="content-section prd-detail industry">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="">Home <i class="fa fa-chevron-right" aria-hidden="true"></i></a></li>
                <li></li>
                <li><a href="">industrial</a></li>
                <li><a href="" class="active"><i class="fa fa-chevron-right" aria-hidden="true"></i>marine industry</a></li>
            </ul>
            <div class="col-sm">
                <h2><?php the_field('industry_title'); ?></h2>
                <p><?php the_field('industry_content'); ?></p>
                <a href="" class="btn-sm"><i class="fa fa-plus-circle" aria-hidden="true"></i>MORE</a>
            </div>
            <div class="col-sm">
          
                <div class="product-slick">
<!--                 
                        <!-- Repeater for Testimonials -->
                            <?php if( have_rows('testimonial') ) : ?>
                                <?php  while ( have_rows('testimonial') ) : the_row(); ?>
                                    <div>
                                        <h3><?php echo the_sub_field('testimonial_title'); ?></h3>
                                    <small><?php echo the_sub_field('testimonial_content'); ?></small>
                                    <a href=""><?php echo the_sub_field('testimonial_author'); ?><br>                    <?php echo the_sub_field('testimonial_author_designation'); ?></a>
                                    <div class="img-bor">
                                        <img src="<?php echo the_sub_field('testimonial_image'); ?>" alt="">
                                    </div>
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>
               </div>
                
            </div>
        </div>
    </div>

    <div class="tabs-box">
        <div class="container">
            <div id="product-tab">
                <ul class="resp-tabs-list">
                        <!-- Repeater for list  -->
                        <?php if( have_rows('resp_product_list') ) : ?>
                            <?php  while ( have_rows('resp_product_list') ) : the_row(); ?>
                               <li><?php the_sub_field('list'); ?></li>
                            <?php endwhile; ?>
                        <?php endif; ?>
                </ul>
                <div class="resp-tabs-container">
                  
                        <?php if( have_rows('resp_tab_container') ) : ?>
                            <?php  while ( have_rows('resp_tab_container') ) : the_row(); ?>
                                <div>
                                <div class="features-box">
                                    <h4><?php the_sub_field('resp_title'); ?></h4>
                                    <p><?php the_sub_field('resp_content'); ?></p>
                                    <ul>
                                        
                                            <?php if( have_rows('feature') ) : ?>
                                                <?php  while ( have_rows('feature') ) : the_row(); ?>
                                                    <li><span><?php the_sub_field('list'); ?></span></li>
                                                <?php endwhile; ?>
                                            <?php endif; ?>

                                    </ul>
                                </div>
                                </div>
                            <?php endwhile; ?>
                         <?php endif; ?>
                  
            
                </div>
            </div>
        </div>
        </div>

<?php get_footer(); ?>