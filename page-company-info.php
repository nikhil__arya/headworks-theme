<?php  
/*
* Template Name: Company Info
*/
?>
<?php get_header(); ?>

<div class="content-section prd-detail">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="">Home</a></li>
                <li><a href="" class="active"><i class="fa fa-chevron-right" aria-hidden="true"></i>Join Our Team</a></li>
            </ul>
			<div class="careers-detail careers-overview">
			<h2><?php  echo the_field('company_title');  ?></h2>
			<div class="careers-text">
			<p><?php the_field('about_company'); ?></p>
			</div>
			<!-- this is for the post publish on this page  --> 
			<?php
			        // the query
			        $the_query = new WP_Query(array(
			            'category_name' => 'company-info',
			            'post_status' => 'publish',
			            'posts_per_page' => 5,
			        ));
			        ?>

			        <?php if ($the_query->have_posts()) : ?>
			            <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
			                <!-- the post title -->
			                <?php the_title(); ?>
			                
			                <!-- the post content -->
			                <?php the_content(); ?>
			            <?php endwhile; ?>
			            <?php wp_reset_postdata(); ?>

			        <?php else : ?>
			            <p><?php __('No News'); ?></p>
			        <?php endif; ?>

			</div>   
            
        </div>
    </div>

<?php get_footer(); ?>