$(".inline").colorbox({inline:true, width:"80%",rel:'group2', transition:"none"});
$(document).ready(function() {
$(".inline").colorbox({inline:true, width:"80%",rel:'group2', transition:"none"});

  $('.menu-toggle').click(function(){
	  $('nav').slideToggle(); 
	 });
	
	
$('nav >li').has('ul').addClass('drop-menu').prepend('<span class="icon open-nav"></span>')
    
 $('nav >li span').click(function() {
   if (!$(this).siblings("ul").is(":visible")) {
     $('nav >li ul').slideUp();
     $('nav >li ul').parent().removeClass('open');
     $(this).parent().addClass('open');
     $(this).siblings("ul").slideDown();
   } else {
     $(this).siblings("ul").slideUp();
     $('nav >li ul').parent().removeClass('open');

   }
 });	
	
$('.slick-bg').slick({
  dots: false,
  infinite: true,
  autoplay: true,
  pauseOnHover: false,
  autoplaySpeed: 9000,
  speed: 800,
  adaptiveHeight: true,
  slidesToShow: 1
  }
);

$('.home-slides').slick({
  dots: false,
  infinite: true,
  autoplay: true,
  pauseOnHover: false,
  autoplaySpeed: 9000,
  
  speed: 800,
  slidesToShow: 1
  }
);




$('.responsive').slick({
  dots: true,
  infinite: false,
  speed: 300,
  slidesToShow: 3,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
		
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }

  ]
});
	
   
$('.one-time').slick({
  dots: true,
  infinite: true,
  speed: 300,
  slidesToShow: 1,
  adaptiveHeight: true
});



$('.product-slick').slick({
  dots: true,
  infinite: true,
  speed: 300,
  slidesToShow: 1,
  adaptiveHeight: true
});

$('header').after('<div class="space"/>')

    $(window).scroll(function() {

        if ($(window).scrollTop() > 1) {

            $('.space1').css('height', $('header').height());

            $('header').addClass('fixed-header');

        } else {

            $('header').removeClass('fixed-header');

            $('.space1').css('height', '0');

        }



    });
	


equalheight = function(a) {
   var e, b = 0,
       c = 0,
       d = new Array;
   $(a).each(function() {
       if (e = $(this), $(e).height("auto"), topPostion = e.position().top, c != topPostion) {
           for (currentDiv = 0; currentDiv < d.length; currentDiv++) d[currentDiv].height(b);
           d.length = 0, c = topPostion, b = e.height(), d.push(e)
       } else d.push(e), b = b < e.height() ? e.height() : b;
       for (currentDiv = 0; currentDiv < d.length; currentDiv++) d[currentDiv].height(b)
   })
}, $(window).on("load resize ready", function() {
   equalheight(".home-company .col-ms")
});




  
 $('#product-tab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion           
            width: 'auto', //auto or any width like 600px
            fit: true,   // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#tabInfo');
                var $name = $('span', $info);

                $name.text($tab.text());

                $info.show();
            }
        }); 

$('.accordian li h5').click(function() {
            if (!$(this).next().is(':visible')) {
                $('.drop').slideUp(400);
                $('.accordian li').removeClass('active');
                $(this).next().slideDown(400);
                $(this).parent().addClass('active');
            }
            else {
                 $('.drop').slideUp(400);
                 $('.accordian li').removeClass('active');
            }
            return false;
     });
	
});

