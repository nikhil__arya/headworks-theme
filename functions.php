<?php 
// function theme_css()
// {
// 	wp_enqueue_style( 'responsive', get_template_directory_uri() . '/css/style-responsive.css' );
// 	wp_enqueue_style( 'style', get_template_directory_uri() . '/css/style.css' );
// 	wp_enqueue_style( 'tabs', get_template_directory_uri() . '/css/tabs.css' );
// }
// add_action( 'wp_enqueue_scripts', 'theme_css' );

// function theme_js() 
//  {
//  	wp_enqueue_script( 'jQuery_ajax', 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js' );
//  	wp_enqueue_script( 'jQuery', 'http://code.jquery.com/jquery-1.7.2.min.js' );
//  	wp_enqueue_script( 'lightbox_js', get_template_directory_uri() . '/js/lightbox.js' );
//  	wp_enqueue_script( 'main_js', get_template_directory_uri() . '/js/main.js' );
//  	wp_enqueue_script( 'tabs_js', get_template_directory_uri() . '/js/tabs.js' );
 	
//  	// wp_enqueue_script( 'bootstrap_js', get_template_directory_uri() . '/js/bootstrap.min.js' );
//  	// wp_enqueue_script( 'theme_js', get_template_directory_uri() . '/js/theme.js', array( 'bootstrap_js' ), '', true );	
//  }
//  add_action( 'wp_enqueue_scripts', 'theme_js' );

add_theme_support( 'menus' );
add_theme_support( 'post_thumbnails' );
add_theme_support( 'post-thumbnails', array( 'post' ) );
add_theme_support( 'post-thumbnails', array( 'page' ) );
add_theme_support( 'post-thumbnails', array( 'post', 'movie' ) ); 

/*
* register nav menu 
*/
function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
      'main-menu' => __( 'Main Menu' ),
    )
  );
}
add_action( 'init', 'register_my_menus' );

/*
* register nav menu 
*/




?>